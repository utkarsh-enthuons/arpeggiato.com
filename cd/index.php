<!DOCTYPE html>
<html lang="en">

<head>
    <?php require_once("../include/head.php")?>
</head>

<body class='scheme_original'>
    <?php require_once("../include/header.php")?>
    <div class="page_content">
        <div class="cd-page">
            <div class="content_wrap">
                <div class="vc_row">
                    <div class="vc_column-container vc_col-sm-12">
                        <div class="vc_column_inner">
                            <div class="sec_title">
                                <h3>Digital Download Only</h3>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="digital_download">
                    <div class="vc_row">
                    <div class="vc_column-container vc_col-sm-4">
                            <div class="vc_column-inner">
                                <div class="digital_box">
                                    <div class="digital_img"><a href="inner.php"><img src="../images/uploads/Youtube-Favorites-CD-Cover-1024x1024.png" alt=""></a></div>
                                    <div class="digital_text">
                                        <h2 class="entry-title"><a href="inner.php">Youtube Favorites, Volume 1</a></h2>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="vc_column-container vc_col-sm-4">
                            <div class="vc_column-inner">
                                <div class="digital_box">
                                    <div class="digital_img"><a href="inner.php"><img src="../images/uploads/Youtube-Favorites-CD-Cover-1024x1024.png" alt=""></a></div>
                                    <div class="digital_text">
                                        <h2 class="entry-title"><a href="inner.php">Youtube Favorites, Volume 1</a></h2>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="vc_column-container vc_col-sm-4">
                            <div class="vc_column-inner">
                                <div class="digital_box">
                                    <div class="digital_img"><a href="inner.php"><img src="../images/uploads/Youtube-Favorites-CD-Cover-1024x1024.png" alt=""></a></div>
                                    <div class="digital_text">
                                        <h2 class="entry-title"><a href="inner.php">Youtube Favorites, Volume 1</a></h2>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="vc_column-container vc_col-sm-4">
                            <div class="vc_column-inner">
                                <div class="digital_box">
                                    <div class="digital_img"><a href="inner.php"><img src="../images/uploads/Youtube-Favorites-CD-Cover-1024x1024.png" alt=""></a></div>
                                    <div class="digital_text">
                                        <h2 class="entry-title"><a href="inner.php">Youtube Favorites, Volume 1</a></h2>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php require_once("../include/footer.php")?>
</body>

</html>