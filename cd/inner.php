<!DOCTYPE html>
<html lang="en">

<head>
    <?php require_once("../include/head.php")?>
</head>

<body class='scheme_original'>
    <?php require_once("../include/header.php")?>
    <div class="page_content">
        <div class="cd_single-product">
            <div class="content_wrap">
                <div class="vc_row">
                    <div class="vc_column-container vc_col-sm-12">
                        <div class="vc_column_inner">
                            <ul class="products columns-3">
                                <li class="product_list">
                                    <div class="post_featured"><a href="" class='hover_icon hover_icon_link'><img
                                                src="../images/uploads/Strung-Up-CD-thumbnail-1-600x600.jpg"
                                                alt=""></a></div>
                                    <div class="post_content">
                                        <h2 class="product_title">Strung Up: Harp & Guitar</h2>
                                        <span class="price">$10.00</span>
                                        <a href="" class="button add_to_cart">Buy Now</a>
                                        <p>Digital Download Only</p>
                                    </div>
                                </li>
                            </ul>
                            <div class="cd-text">
                                <p>A CD of 20th-century minimalist music arranged for the harp and guitar. Music by Glass, Pärt, and Hovhaness. All music arranged by Keryn Wouden (harp) and Brandon Acker (classical guitar).</p>
                            </div>
                            <div class="cd-product-text">
                                <ol>
                                    <li>Für Alina by Arvo Pärt</li>
                                    <li>2 – 7. String Quartet No. 3 “Mishima” by Philip Glass</li>
                                    <li>Fratres by Arvo Pärt</li>
                                    <li>Escape by Philip Glass</li>
                                    <li>The Hours by Philip Glass</li>
                                    <li>11 – 15. Sonata for Harp and Guitar, Op. 374, “Spirit of Trees” by Alan Hovhaness</li>
                                </ol>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php require_once("../include/footer.php")?>
</body>

</html>