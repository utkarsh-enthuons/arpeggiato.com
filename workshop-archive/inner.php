<!DOCTYPE html>
<html lang="en">

<head>
    <?php require_once("../include/head.php")?>
</head>

<body class='scheme_original'>
    <?php require_once("../include/header.php")?>
    <div class="page_content_wrap master_inner">
    <div class="cd_single-product">
            <div class="content_wrap">
                <div class="vc_row">
                    <div class="vc_column-container vc_col-sm-12">
                        <div class="vc_column_inner">
                            <h1>To view this video, click on Buy Now</h1>
                            <ul class="products columns-3">
                                <li class="product_list">
                                    <div class="post_featured"><a href="" class="hover_icon hover_icon_link"><img src="../images/uploads/Arpeggiato-Masterclass-1-Thumbnail-600x338.png" alt=""></a></div>
                                    <div class="post_content">
                                        <h2 class="product_title">Masterclass #1</h2>
                                        <a href="" class="button add_to_cart">Buy Now</a>
                                    </div>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php require_once("../include/footer.php")?>
</body>

</html>