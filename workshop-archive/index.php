<!DOCTYPE html>
<html lang="en">

<head>
    <?php require_once("../include/head.php")?>
</head>

<body class='scheme_original'>
    <?php require_once("../include/header.php")?>
    <div class="page_content">
        <div class="masterclass_archive">
            <div class="content_wrap">
                <div class="vc_row">
                    <div class="vc_column-container vc_col-sm-12">
                        <div class="vc_column_inner">
                            <div class="archive_title">
                                <div class="sec_title">
                                    <h3>PAST MASTERCLASSES</h3>
                                </div>
                                <h4> $12 ($5 FOR STUDENTS)</h4>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="vc_row">
                    <div class="vc_col-sm-4 vc_column-container">
                        <div class="vc_column-inner">
                            <div class="digital_box">
                                <div class="digital_img"><a href="inner.php"><img src="../images/uploads/masterclass-6-redo-thumbnail-1024x576.png" alt=""></a></div>
                                <div class="digital_text">
                                    <h2 class="entry-title"><a href="">Guitar class #1</a></h2>
                                    <div class="read_more"><a href="">Watch</a></div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="vc_col-sm-4 vc_column-container">
                        <div class="vc_column-inner">
                            <div class="digital_box">
                                <div class="digital_img"><a href="inner.php"><img src="../images/uploads/masterclass-6-redo-thumbnail-1024x576.png" alt=""></a></div>
                                <div class="digital_text">
                                    <h2 class="entry-title"><a href="">Guitar class #2</a></h2>
                                    <div class="read_more"><a href="">Watch</a></div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php require_once("../include/footer.php")?>
</body>

</html>