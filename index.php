<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="description" content="">
    <meta name="keywords" content="">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Arpeggiato</title>
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>  
    <![endif]-->
    <link rel="stylesheet" href="css/style.css">
    <link rel="stylesheet" href="css/theme.css">
    <link rel="stylesheet" href="css/responsive.css">
    <link rel="stylesheet" href="css/animate.css">
</head>

<body>
    <div id="index">
        <div class="vc_row wpb_row vc_row-fluid vc_row-has-fill vc_row-no-padding vc_row-o-full-height vc_row-o-columns-middle vc_row-flex">
            <div class="wpb_column vc_column_container vc_col-sm-3">
                <div class="vc_column-inner">
                    <div class="wpb_wrapper"></div>
                </div>
            </div>
            <div class="wpb_column vc_column_container vc_col-sm-6">
                <div class="vc_column-inner">
                    <div class="wpb_wrapper">
                        <div class="wpb_single_image wpb_content_element vc_align_center  wpb_animate_when_almost_visible wpb_fadeIn fadeIn wpb_start_animation animated">
                            <figure class="wpb_wrapper vc_figure">
                                <div class="vc_single_image-wrapper vc_box_border_grey">
                                    <img src="images/uploads/Arpeggiato_Logo_White-160x300.png" class="vc_single_image-img attachment-medium" alt="" loading="lazy">
                            </div>
                            </figure>
                        </div>
                        <div class="vc_btn3-container fadeIn vc_btn3-center animated"
                            id="startbtn"><a style="background-color:#953735; color:#ffffff;"
                                class="vc_general vc_btn3 vc_btn3-size-md vc_btn3-shape-round vc_btn3-style-custom"
                                href="home/index.php" title="">START LEARNING TODAY!</a></div>
                    </div>
                </div>
            </div>
            <div class="wpb_column vc_column_container vc_col-sm-3">
                <div class="vc_column-inner">
                    <div class="wpb_wrapper"></div>
                </div>
            </div>
        </div>
        <?php require_once("include/footer.php")?>
    </div>
</body>
</html>