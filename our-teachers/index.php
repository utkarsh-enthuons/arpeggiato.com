<!DOCTYPE html>
<html lang="en">

<head>
    <?php require_once("../include/head.php")?>
</head>

<body class='scheme_original'>
    <?php require_once("../include/header.php")?>
    <section class="page_content_wrap">
        <div class="vc_row vc_row-no-padding">
            <div class="vc_col-sm-12 vc_column_container">
                <div class="vc_column-inner">
                    <h2 style='text-align: center'>Our Teachers</h2>
                    <div class="customeffects">
                        <ul>
                        <li><a href="">
                                    <img src="../images/uploads/JONATHAN_SquarePhoto.jpg" class='teachboximg' alt="">
                                    <div class="teachboxtext">
                                        <h4>JONATHAN</h4>
                                        <p>Early Plucked Instruments</p>
                                        <p>Classical &amp; Acoustic Guitar</p>
                                        <p>Fingerstyle Ukulele</p>
                                        <p>Sitar</p>
                                        <p>Music Theory</p>
                                        <p>Bass &amp; Electric Guitar</p>
                                    </div>
                                </a>
                            </li>
                            <li><a href="">
                                    <img src="../images/uploads/JONATHAN_SquarePhoto.jpg" class='teachboximg' alt="">
                                    <div class="teachboxtext">
                                        <h4>JONATHAN</h4>
                                        <p>Early Plucked Instruments</p>
                                        <p>Classical &amp; Acoustic Guitar</p>
                                        <p>Fingerstyle Ukulele</p>
                                        <p>Sitar</p>
                                        <p>Music Theory</p>
                                        <p>Bass &amp; Electric Guitar</p>
                                    </div>
                                </a>
                            </li>
                            <li><a href="">
                                    <img src="../images/uploads/JONATHAN_SquarePhoto.jpg" class='teachboximg' alt="">
                                    <div class="teachboxtext">
                                        <h4>JONATHAN</h4>
                                        <p>Early Plucked Instruments</p>
                                        <p>Classical &amp; Acoustic Guitar</p>
                                        <p>Fingerstyle Ukulele</p>
                                        <p>Sitar</p>
                                        <p>Music Theory</p>
                                        <p>Bass &amp; Electric Guitar</p>
                                    </div>
                                </a>
                            </li>
                            <li><a href="">
                                    <img src="../images/uploads/JONATHAN_SquarePhoto.jpg" class='teachboximg' alt="">
                                    <div class="teachboxtext">
                                        <h4>JONATHAN</h4>
                                        <p>Early Plucked Instruments</p>
                                        <p>Classical &amp; Acoustic Guitar</p>
                                        <p>Fingerstyle Ukulele</p>
                                        <p>Sitar</p>
                                        <p>Music Theory</p>
                                        <p>Bass &amp; Electric Guitar</p>
                                    </div>
                                </a>
                            </li>
                            <li><a href="">
                                    <img src="../images/uploads/JONATHAN_SquarePhoto.jpg" class='teachboximg' alt="">
                                    <div class="teachboxtext">
                                        <h4>JONATHAN</h4>
                                        <p>Early Plucked Instruments</p>
                                        <p>Classical &amp; Acoustic Guitar</p>
                                        <p>Fingerstyle Ukulele</p>
                                        <p>Sitar</p>
                                        <p>Music Theory</p>
                                        <p>Bass &amp; Electric Guitar</p>
                                    </div>
                                </a>
                            </li>
                            <li><a href="">
                                    <img src="../images/uploads/JONATHAN_SquarePhoto.jpg" class='teachboximg' alt="">
                                    <div class="teachboxtext">
                                        <h4>JONATHAN</h4>
                                        <p>Early Plucked Instruments</p>
                                        <p>Classical &amp; Acoustic Guitar</p>
                                        <p>Fingerstyle Ukulele</p>
                                        <p>Sitar</p>
                                        <p>Music Theory</p>
                                        <p>Bass &amp; Electric Guitar</p>
                                    </div>
                                </a>
                            </li>
                            <li><a href="">
                                    <img src="../images/uploads/JONATHAN_SquarePhoto.jpg" class='teachboximg' alt="">
                                    <div class="teachboxtext">
                                        <h4>JONATHAN</h4>
                                        <p>Early Plucked Instruments</p>
                                        <p>Classical &amp; Acoustic Guitar</p>
                                        <p>Fingerstyle Ukulele</p>
                                        <p>Sitar</p>
                                        <p>Music Theory</p>
                                        <p>Bass &amp; Electric Guitar</p>
                                    </div>
                                </a>
                            </li>
                            <li><a href="">
                                    <img src="../images/uploads/JONATHAN_SquarePhoto.jpg" class='teachboximg' alt="">
                                    <div class="teachboxtext">
                                        <h4>JONATHAN</h4>
                                        <p>Early Plucked Instruments</p>
                                        <p>Classical &amp; Acoustic Guitar</p>
                                        <p>Fingerstyle Ukulele</p>
                                        <p>Sitar</p>
                                        <p>Music Theory</p>
                                        <p>Bass &amp; Electric Guitar</p>
                                    </div>
                                </a>
                            </li>
                            <li><a href="">
                                    <img src="../images/uploads/JONATHAN_SquarePhoto.jpg" class='teachboximg' alt="">
                                    <div class="teachboxtext">
                                        <h4>JONATHAN</h4>
                                        <p>Early Plucked Instruments</p>
                                        <p>Classical &amp; Acoustic Guitar</p>
                                        <p>Fingerstyle Ukulele</p>
                                        <p>Sitar</p>
                                        <p>Music Theory</p>
                                        <p>Bass &amp; Electric Guitar</p>
                                    </div>
                                </a>
                            </li>
                            <li><a href="">
                                    <img src="../images/uploads/JONATHAN_SquarePhoto.jpg" class='teachboximg' alt="">
                                    <div class="teachboxtext">
                                        <h4>JONATHAN</h4>
                                        <p>Early Plucked Instruments</p>
                                        <p>Classical &amp; Acoustic Guitar</p>
                                        <p>Fingerstyle Ukulele</p>
                                        <p>Sitar</p>
                                        <p>Music Theory</p>
                                        <p>Bass &amp; Electric Guitar</p>
                                    </div>
                                </a>
                            </li>
                            <li><a href="">
                                    <img src="../images/uploads/JONATHAN_SquarePhoto.jpg" class='teachboximg' alt="">
                                    <div class="teachboxtext">
                                        <h4>JONATHAN</h4>
                                        <p>Early Plucked Instruments</p>
                                        <p>Classical &amp; Acoustic Guitar</p>
                                        <p>Fingerstyle Ukulele</p>
                                        <p>Sitar</p>
                                        <p>Music Theory</p>
                                        <p>Bass &amp; Electric Guitar</p>
                                    </div>
                                </a>
                            </li>
                            <li><a href="../teachersbook/jonathan/index.html">
                                    <img src="../images/uploads/JONATHAN_SquarePhoto.jpg" class='teachboximg' alt="">
                                    <div class="teachboxtext">
                                        <h4>JONATHAN</h4>
                                        <p>Early Plucked Instruments</p>
                                        <p>Classical &amp; Acoustic Guitar</p>
                                        <p>Fingerstyle Ukulele</p>
                                        <p>Sitar</p>
                                        <p>Music Theory</p>
                                        <p>Bass &amp; Electric Guitar</p>
                                    </div>
                                </a>
                            </li>
                            <li><a href="">
                                    <img src="../images/uploads/JONATHAN_SquarePhoto.jpg" class='teachboximg' alt="">
                                    <div class="teachboxtext">
                                        <h4>JONATHAN</h4>
                                        <p>Early Plucked Instruments</p>
                                        <p>Classical &amp; Acoustic Guitar</p>
                                        <p>Fingerstyle Ukulele</p>
                                        <p>Sitar</p>
                                        <p>Music Theory</p>
                                        <p>Bass &amp; Electric Guitar</p>
                                    </div>
                                </a>
                            </li>
                        </ul>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <?php require_once("../include/footer.php")?>
</body>

</html>