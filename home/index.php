<!DOCTYPE html>
<html lang="en">

<head>
<?php require_once("../include/head.php")?>

</head>

<body class='scheme_original'>
    <?php require_once("../include/header.php")?>
    <section class="banner">
        <div class="banner_box">
            <img src="../images/uploads/new-arp-6.jpg" alt="">
            <div class="banner_content">
                <div class="content_wrap">
                    <div class="text">All things that go pluck!</div>
                    <a href="" class="btn learning">Start learning</a>
                </div>
            </div>
        </div>
    </section>
    <div class="accent1_bg private_lessons sc_call_to_action_style_2">
        <div class="content_wrap">
            <div class="vc_row wpb_row">
                <div class="wpb_column vc_column_container vc_col-sm-12">
                    <div class="vc_column-inner">
                        <div class="wpb_wrapper">
                            <div class="columns_wrap">
                                <div class="column-3_4">
                                    <h1 class="sc_call_to_action_title sc_item_title">Private Lessons</h1>
                                    <div class="sc_call_to_action_descr sc_item_descr">We offer a high-quality music
                                        education online for individuals of just about every age and skill
                                        level.<br>Click on HOW IT WORKS or FAQ to learn more.</div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

    <div class="video" id='virtual-lessons-section'>
        <div class="content_wrap">
            <div class="vc_row">
                <div class="vc_col-sm-12 vc_column_container">
                    <div class="vc_column-inner">
                        <h1 class="sc_section_title sc_item_title">How does Arpeggiato work?</h1>
                        <h6 class="sc_section_subtitle sc_item_subtitle">video</h6>
                        <iframe style='width: 100%; height: 563px' height='315'
                            src="https://www.youtube.com/embed/mm03ak_4C_Y" title="YouTube video player" frameborder="0"
                            allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture"
                            allowfullscreen></iframe>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div class="content_wrap">
        <div class="instrument">
            <div id="choose-your-style-sec" class="vc_row">
                <div class="vc_column-inner">
                    <h1 class="sc_section_title sc_item_title">Choose Your Instrument</h1>
                </div>
            </div>
            <div class="vc_row">
                <div class="vc_column_container vc_col-sm-4">
                    <div class="vc_column-inner">
                        <div class="wpb_wrapper">
                            <div class="vc_btn3-container btn_design vc_btn3-center"><a href=""
                                    class="vc_general vc_btn3 vc_btn3-shape-round vc_btn3-style-custom vc_btn3-block">Classical
                                    Guitar</a></div>
                        </div>
                    </div>
                </div>
                <div class="vc_column_container vc_col-sm-4">
                    <div class="vc_column-inner">
                        <div class="wpb_wrapper">
                            <div class="vc_btn3-container btn_design vc_btn3-center"><a href=""
                                    class="vc_general vc_btn3 vc_btn3-shape-round vc_btn3-style-custom vc_btn3-block">Classical
                                    Guitar</a></div>
                        </div>
                    </div>
                </div>
                <div class="vc_column_container vc_col-sm-4">
                    <div class="vc_column-inner">
                        <div class="wpb_wrapper">
                            <div class="vc_btn3-container btn_design vc_btn3-center"><a href=""
                                    class="vc_general vc_btn3 vc_btn3-shape-round vc_btn3-style-custom vc_btn3-block">Classical
                                    Guitar</a></div>
                        </div>
                    </div>
                </div>
                <div class="vc_column_container vc_col-sm-4">
                    <div class="vc_column-inner">
                        <div class="wpb_wrapper">
                            <div class="vc_btn3-container btn_design vc_btn3-center"><a href=""
                                    class="vc_general vc_btn3 vc_btn3-shape-round vc_btn3-style-custom vc_btn3-block">Classical
                                    Guitar</a></div>
                        </div>
                    </div>
                </div>
                <div class="vc_column_container vc_col-sm-4">
                    <div class="vc_column-inner">
                        <div class="wpb_wrapper">
                            <div class="vc_btn3-container btn_design vc_btn3-center"><a href=""
                                    class="vc_general vc_btn3 vc_btn3-shape-round vc_btn3-style-custom vc_btn3-block">Classical
                                    Guitar</a></div>
                        </div>
                    </div>
                </div>
                <div class="vc_column_container vc_col-sm-4">
                    <div class="vc_column-inner">
                        <div class="wpb_wrapper">
                            <div class="vc_btn3-container btn_design vc_btn3-center"><a href=""
                                    class="vc_general vc_btn3 vc_btn3-shape-round vc_btn3-style-custom vc_btn3-block">Classical
                                    Guitar</a></div>
                        </div>
                    </div>
                </div>
                <div class="vc_column_container vc_col-sm-4">
                    <div class="vc_column-inner">
                        <div class="wpb_wrapper">
                            <div class="vc_btn3-container btn_design vc_btn3-center"><a href=""
                                    class="vc_general vc_btn3 vc_btn3-shape-round vc_btn3-style-custom vc_btn3-block">Classical
                                    Guitar</a></div>
                        </div>
                    </div>
                </div>
                <div class="vc_column_container vc_col-sm-4">
                    <div class="vc_column-inner">
                        <div class="wpb_wrapper">
                            <div class="vc_btn3-container btn_design vc_btn3-center"><a href=""
                                    class="vc_general vc_btn3 vc_btn3-shape-round vc_btn3-style-custom vc_btn3-block">Classical
                                    Guitar</a></div>
                        </div>
                    </div>
                </div>
                <div class="vc_column_container vc_col-sm-4">
                    <div class="vc_column-inner">
                        <div class="wpb_wrapper">
                            <div class="vc_btn3-container btn_design vc_btn3-center"><a href=""
                                    class="vc_general vc_btn3 vc_btn3-shape-round vc_btn3-style-custom vc_btn3-block">Classical
                                    Guitar</a></div>
                        </div>
                    </div>
                </div>
                <div class="vc_column_container vc_col-sm-4">
                    <div class="vc_column-inner">
                        <div class="wpb_wrapper">
                            <div class="vc_btn3-container btn_design vc_btn3-center"><a href=""
                                    class="vc_general vc_btn3 vc_btn3-shape-round vc_btn3-style-custom vc_btn3-block">Classical
                                    Guitar</a></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="music-section" class='vc_row vc_row-no-padding'>
        <div class="wpb_column vc_column_container vc_col-sm-12">
            <div class="vc_column-inner">
                <div class="sc_parallax">
                    <div class="sc_parallax_content"
                        style='background-image: url("../images/uploads/new-arp-4.jpg"); background-position: 50% 50%;'>
                        <div class="content-wrap">
                            <h1 class="sc_title">ARPEGGIATO MUSIC SCHOOL</h1>
                            <h5>We employ experts in a variety of instruments and musical styles from around the globe.</h5>
                            <p></p>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="next-event" class="accent1_bg events">
        <div data-vc-full-width="true" class="vc_row content_wrap">
            <div class="vc_column_container vc_col-sm-12">
                <div class="vc_column-inner">
                    <div class="sc_section alignleft">
                        <div class="sc_section_inner">
                            <h1>Next Event</h1>
                        </div>
                    </div>
                    <div class='sc_section margin_top_null margin_left_small alignleft'>
                        <div class="sc_section-inner">
                            <h3 class="sc_title">SEPT. 11th 2021</h3>
                        </div>
                    </div>
                    <div class='sc_section margin_left_medium alignleft' style="max-width: 550px;">
                        <div class="sc_section-inner">
                            <div style='margin-top: 40px'>We’re excited to announce our Guitar Workshop #4!</div>
                            <p>This workshop is open to everyone, not only our students.</p>
                            <p>To view our previous Masterclasses & Workshops, please visit our <a href="">e-shop</a>.</p>
                        </div>
                    </div>
                    <div class="sc_section alignleft">
                        <div class="sc_section_inner">
                            <a href="" class="sc_button sc_button_square sc_button_style_filled sc_button_size_medium style_color_dark_hover">Learn More</a>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="latest_news" class="content_wrap">
        <div class="vc_row">
            <div class="vc_column_container vc_col-sm-12">
                <div class="vc_column-inner">
                    <div class="sc_section-inner">
                        <h1 class="sc_section_title sc_item_title">Latest News</h1>
                        <h6 class="sc_section_subtitle sc_item_subtitle">Arpeggiato</h6>
                        <div id="blogger">
                            <div class="vc_row">
                                <div class="vc_col-sm-4">
                                    <div class="blogger_box">
                                        <div class="blogger_img">
                                            <a href="" class='hover_icon hover_icon_link'>
                                                <img src="../images/uploads/Arpeggiato-full-team-Small600.png" alt="">
                                            </a>
                                        </div>
                                        <div class="post_info"> <a href="../here-we-are/index.html" class="post_info_date">September 24, 2020</a></div>
                                        <h3 class="post_title"><a href="">Our team!</a></h3>
                                        <p>We’re very excited to present the full team of Arpeggiato! From top to bottom, and from left to right: Deanna, Mustafa, Salome, Jeff...</p>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <div id="news_form">
        <div class="vc_row content_wrap" data-vc-full-width="true">
            <div class="vc_col-sm-12">
                <div class="vc_column-inner">
                    <h2>Receive Arpeggiato News</h2>
                    <div class="newsletter-form-row">
                        <p><input type="text" class="form-control" placeholder='First Name'></p>
                        <p><input type="text" class="form-control" placeholder='Last Name'></p>
                        <p><input type="text" class="form-control" placeholder='Email'></p>
                        <p><img src="https://arpeggiato.com/wp-content/uploads/wpcf7_captcha/3422570552.png" width='72' height='24' alt=""><input type="text" class="form-control" placeholder='Type the Code Shown'> <br><br><br> <input type="submit" value='Sign Up' class="btn btn-submit"></p>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php require_once("../include/footer.php")?>
</body>

</html>