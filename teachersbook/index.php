<!DOCTYPE html>
<html lang="en">

<head>
    <?php require_once("../include/head.php")?>
</head>

<body class='scheme_original'>
    <?php require_once("../include/header.php")?>
    <section class="page_content_wrap">
        <div class="content_wrap">
        <div class="vc_row">
            <div class="vc_col-sm-5 vc_column_container">
                <img src="../images/uploads/JONATHAN_SquarePhoto.jpg" alt="">
                <h6><strong>JONATHAN teaches Early Plucked instruments (theorbo, guitars, vihuela, lutes), Classical / Acoustic (Fingerstyle) guitar, Ukulele, Music Theory, Songwriting, Bass and Electric guitar, Music Theory, all levels. Jon also offers beginner Sitar/Indian Classical Music classes. <br>He can teach in English and Spanish. <br>Each lesson is 55 minutes.</strong></h6>
                <p>Jon is a specialist in plucked string instruments, is a native of New York, and currently based in Madrid. He started his musical studies at the age of 10. He holds a Bachelor’s Degree in Classical Guitar and Ethnomusicology and a Masters Degree in Historical Plucked String Instruments, both from the Jacobs School of Music at Indiana University as well as, the completion of the “formación continuada” program at Escola Superior de Music de Catalunya.</p>
                <p dir="ltr">Jon has been teaching private and classroom lessons for over 8 years. Having a diverse musical background and upbringing allows him to tailor lessons for individual students of all ages, musical interests, and skill levels.</p>
                <p dir="ltr">He has studied with world-renowned lutenists such as Nigel North and Xavier Diaz Lattore. His previous teachers include Petar Jankovic, Christopher Gotzenberg (Classical Guitar), Ikhlaq Hussain (Sitar), and Steven Rickards (Voice).</p>
                <p dir="ltr">Jon has experience performing historical plucked string instruments, such as theorbo, lutes, vihuela, 5-course guitar, and 4-course guitar, in addition to acoustic/electric/classical guitars, mandolin, banjo, ukulele, sitar, and bass.</p>
                <p><iframe title="Jon at Arpeggiato" width="1170" height="658" src="https://www.youtube.com/embed/mniGCaOAoJc?feature=oembed" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen="" style="width: 100%; height: 230px;"></iframe></p>
                <h6>Ask a question </h6>
                <form action="" class="mail">
                    <p>
                        <label for="">Your email (required)</label>
                        <input type="text" class="form-control">
                    </p>
                    <p>
                        <label for="">Your Name</label>
                        <input type="text" class="form-control">
                    </p>
                    <p>
                        <label for="">Your Message</label>
                        <textarea name="" id="" rows="3" class="form-control"></textarea>
                    </p>
                    <p>
                        <label for="">Type the code shown.</label>
                        <img src="https://arpeggiato.com/wp-content/uploads/wpcf7_captcha/647763859.png" alt="">
                        <input type="text" class="form-control">
                    </p>
                    <input type="submit" value="Send" class="btn btn-submit">
                </form>
            </div>
            <div class="vc_col-sm-5 vc_column_container">
                <div class="vc_column-inner">
                    <div class="summary">
                        <h1 class="product_title">YOUR WEEKLY TEACHER, JONATHAN:</h1>
                        <p class="price">$60.00/lesson</p>
                        <img src="../images/uploads/capture.png" alt="">
                        <table border="0" cellpadding="0" cellspacing="0" class="month"> <tr><th colspan="7" class="month">August 2021</th></tr> <tr><th class="mon">Mon</th><th class="tue">Tue</th><th class="wed">Wed</th><th class="thu">Thu</th><th class="fri">Fri</th><th class="sat">Sat</th><th class="sun">Sun</th></tr> <tr><td class="noday">&nbsp;</td><td class="noday">&nbsp;</td><td class="noday">&nbsp;</td><td class="noday">&nbsp;</td><td class="noday">&nbsp;</td><td class="noday">&nbsp;</td><td class="sun">1</td></tr> <tr><td class="mon">2</td><td class="tue">3</td><td class="wed">4</td><td class="thu">5</td><td class="fri">6</td><td class="sat">7</td><td class="sun">8</td></tr> <tr><td class="mon">9</td><td class="tue">10</td><td class="wed">11</td><td class="thu">12</td><td class="fri">13</td><td class="sat">14</td><td class="sun">15</td></tr> <tr><td class="mon">16</td><td class="tue">17</td><td class="wed">18</td><td class="thu">19</td><td class="fri">20</td><td class="sat">21</td><td class="sun">22</td></tr> <tr><td class="mon">23</td><td class="tue">24</td><td class="wed">25</td><td class="thu">26</td><td class="fri">27</td><td class="sat">28</td><td class="sun">29</td></tr> <tr><td class="mon">30</td><td class="tue">31</td><td class="noday">&nbsp;</td><td class="noday">&nbsp;</td><td class="noday">&nbsp;</td><td class="noday">&nbsp;</td><td class="noday">&nbsp;</td></tr> </table>
                        <form action="" class="cart">
                            <h6><strong>Not finding the right time-slot for you? Please <a href="../contact/index.html">contact us!</a></strong></h6>
                            <div>Pack: <select name="" id="">
                                <option value="">4-lessons pack : $240</option>
                                <option value="">8-lessons pack : $460 (You save $20)</option>
                                <option value="">12-lessons pack : $680 (You save $40)</option>
                            </select></div>
                            <button class="btn">Book Now</button>    
                        </form>
                        <div class="product_meta">
                            <span class="posted_in">Categories: <a href="../../music-category/bass/index.html" rel="tag">Bass</a>, <a href="../../music-category/early-music-instruments/index.html" rel="tag">Early Music Instruments</a>, <a href="../../music-category/fingerstyle/index.html" rel="tag">Fingerstyle</a>, <a href="../../music-category/guitar/index.html" rel="tag">Guitar</a>, <a href="../../music-category/music-theory/index.html" rel="tag">Music Theory</a>, <a href="../../music-category/sitar/index.html" rel="tag">Sitar</a>, <a href="../../music-category/ukulele/index.html" rel="tag">Ukulele</a></span>
                        </div>
                        <h6 class="custthirdparty"><a href="" class='btn' target='_blank'>Check Time Zone</a></h6>
                    </div>
                    <div class="woocommerce-tabs">
                        <ul class="tabs wc-tabs" role='tablist'>
                        <li class="reviews_tab active" id="tab-title-reviews" role="tab" aria-controls="tab-reviews">
					        <a href="#tab-reviews">REVIEWS (2)</a>
				        </li>
                        </ul>
                        <div class="woocommerce-Tabs-panel">
                            <div id="comments">
                                <ol class="commentlist">
                                    <li class="comment">
                                        <div class="comment-container">
                                            <img src="../images/uploads/Arpeggiato_Logo_black-1-60x60.png"  class='avatar' alt="">
                                            <div class="comment-text">
                                                <p class="meta"><strong class="author">Heath, 37, Indiana </strong> - <span class="publish_date">March 27, 2021</span></p>
                                                <div class="description"><p>Jon is an absolutely excellent guitar and music instructor.  His knowledge of both theory and technique is immense, and he conveys both to his students in fun and engaging ways! I thoroughly enjoyed learning from him, and would recommend his instruction to anyone looking to learn music theory, guitar, and a variety of other stringed instruments!</p></div>
                                            </div>
                                        </div>
                                    </li>
                                    <li class="comment">
                                        <div class="comment-container">
                                            <img src="../images/uploads/Arpeggiato_Logo_black-1-60x60.png"  class='avatar' alt="">
                                            <div class="comment-text">
                                                <p class="meta"><strong class="author">Heath, 37, Indiana </strong> - <span class="publish_date">March 27, 2021</span></p>
                                                <div class="description"><p>Jon is an absolutely excellent guitar and music instructor.  His knowledge of both theory and technique is immense, and he conveys both to his students in fun and engaging ways! I thoroughly enjoyed learning from him, and would recommend his instruction to anyone looking to learn music theory, guitar, and a variety of other stringed instruments!</p></div>
                                            </div>
                                        </div>
                                    </li>
                                </ol>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        </div>
    </section>
    <?php require_once("../include/footer.php")?>
</body>

</html>