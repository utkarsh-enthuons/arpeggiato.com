<!DOCTYPE html>
<html lang="en">

<head>
    <?php require_once("../include/head.php")?>
</head>

<body class='scheme_original'>
    <?php require_once("../include/header.php")?>
    <div class="page_content">
        <div class="about_banner">
        </div>
        <div class="about_heading">
            <div class="content_wrap">
                <p>Meet Meriem & Brandon, the co-founders of Arpeggiato!</p>
            </div>
        </div>
        <div class="about_content">
            <div class="content_wrap">
                <div class="vc_row">
                    <div class="vc_column_container vc_col-sm-6">
                        <div class="vc_column-inner">
                            <h4>Brandon Acker</h4>
                            <div class="vc_separator"></div>
                            <p>Brandon Acker is a musician who plays “All things that go pluck,” with an emphasis on guitars and early plucked instruments like the theorbo and lute. Besides his active performance career which has led him to tour extensively around the UK, US and Canada, he has released three CDs, received a first place prize in a guitar competition and played with notable companies such as: the Joffrey Ballet, the Chicago Philharmonic, Leipzig Baroque Orchestra, the Newberry Consort, Haymarket Opera Company, Music of the Baroque, Piffaro and more.<br>Brandon’s passion for teaching has led him to create an educational Youtube channel about guitars and historical instruments, with a following of over 200,000 subscribers and millions of views. In hope of providing a high quality music education to as wide an audience as possible, he and his wife have started their own online music school: Arpeggiato.</p>
                            <div class="share">
                                <div class="vc_row">
                                    <div class="vc_col-sm-2 vc_column-container">
                                        <div class="vc_column-inner"><a href="" class="sc_icon icon-facebook aligncenter"></a></div>
                                    </div>
                                    <div class="vc_col-sm-2 vc_column-container">
                                        <div class="vc_column-inner"><a href="" class="sc_icon icon-instagramm aligncenter"></a></div>
                                    </div>
                                    <div class="vc_col-sm-2 vc_column-container">
                                        <div class="vc_column-inner"><a href="" class="sc_icon icon-youtube aligncenter"></a></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                    <div class="vc_column_container vc_col-sm-6">
                        <div class="vc_column-inner">
                            <h4>Brandon Acker</h4>
                            <div class="vc_separator"></div>
                            <p>Brandon Acker is a musician who plays “All things that go pluck,” with an emphasis on guitars and early plucked instruments like the theorbo and lute. Besides his active performance career which has led him to tour extensively around the UK, US and Canada, he has released three CDs, received a first place prize in a guitar competition and played with notable companies such as: the Joffrey Ballet, the Chicago Philharmonic, Leipzig Baroque Orchestra, the Newberry Consort, Haymarket Opera Company, Music of the Baroque, Piffaro and more.<br>Brandon’s passion for teaching has led him to create an educational Youtube channel about guitars and historical instruments, with a following of over 200,000 subscribers and millions of views. In hope of providing a high quality music education to as wide an audience as possible, he and his wife have started their own online music school: Arpeggiato.</p>
                            <div class="share">
                                <div class="vc_row">
                                    <div class="vc_col-sm-2 vc_column-container">
                                        <div class="vc_column-inner"><a href="" class="sc_icon icon-facebook aligncenter"></a></div>
                                    </div>
                                    <div class="vc_col-sm-2 vc_column-container">
                                        <div class="vc_column-inner"><a href="" class="sc_icon icon-instagramm aligncenter"></a></div>
                                    </div>
                                    <div class="vc_col-sm-2 vc_column-container">
                                        <div class="vc_column-inner"><a href="" class="sc_icon icon-youtube aligncenter"></a></div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php require_once("../include/footer.php")?>
</body>
</html>