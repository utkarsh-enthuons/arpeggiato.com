<!DOCTYPE html>
<html lang="en">

<head>
    <?php require_once("../include/head.php")?>
</head>

<body class='scheme_original'>
    <?php require_once("../include/header.php")?>
    <div class="page_content">
        <div class="how_it_works">
            <div class="content_wrap">
                <div class="vc_row" data-vc-full-width="true">
                    <div class="vc_col-sm-12 vc_column-container">
                        <div class="vc_column-inner">
                            <div class="how_text">
                                <h1>HOW IT WORKS</h1>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="hows_content">
            <div class="content_wrap">
                <div class="vc_row">
                    <div class="vc_col-sm-12 vc_column-container">
                        <div class="vc_column-inner">
                            <h1>How Our Lessons Work:</h1>
                            <ul>
                                <li><span>Step 1:</span> Choose one of our expert teachers for your weekly lessons. If
                                    you have difficulties choosing, feel free to write to us for a recommendation based
                                    on your needs.</li>
                                <li><span>Step 2:</span> In each teacher’s bio, you’ll find a calendar with their
                                    availability. Lessons come in 4, 8, or 12 packs.</li>
                                <li><span>Step 3:</span> In the calendar, simply click on your preferred day within the
                                    two upcoming weeks. Then choose which lesson slot you’d prefer based on the
                                    teacher’s availability. Finally, click “Book Now.”</li>
                                <li><span>Step 4:</span> Next, you’ll be brought to the check-out window where you can
                                    pay for your pack of lessons by using Paypal or Credit Card.</li>
                                <li><span>Step 5:</span> You will receive an email confirming your lesson appointment
                                    within 48 hours of your purchase that further details about your weekly lessons. The
                                    day/time-slot you booked is yours until you stop lessons.</li>
                            </ul>
                            <h2>Lessons Start at $60/lesson*</h2>
                            <p>*Some teachers might have higher rates*</p>
                            <h4>Lessons can be purchased in packs of 4 lessons at a time. Each lesson lasts 55 minutes.
                                We do not offer single lessons at this time. Our 4 pack starts at $240.</h4>
                            <h2>All Packs Include:</h2>
                            <h4>– Discounts on masterclasses and workshops directed by Brandon Acker.</h4>
                            <h4>– Access to an online Arpeggiato community on Facebook.</h4>
                            <h4>– Weekly lessons from expert music instructors.</h4>
                            <h2>Other Information:</h2>
                            <h4>– Please ensure you have a laptop/tablet/phone which can connect to the internet.</h4>
                            <h4>– All lessons will take place on Zoom. Please sign-up for a free account and download
                                Zoom prior to your first lesson.</h4>
                            <h4>– Please note: if the lesson time you’ve selected is within 48h of the time of purchase,
                                your first lesson will take place the following week.</h4>
                            <h4>This gives our teachers the time to prepare your lesson.</h4>
                            <h4>Example: if on Monday at 5 pm you select a lesson slot of Thursdays at 5 pm, your first
                                lesson will take place the same week.</h4>
                            <h4>However, if on Monday at 5 pm you select a lesson slot of Tuesdays at 5 pm, your first
                                lesson will take place the following week.</h4>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="how_teachers">
            <div class="vc_row vc_row-no-padding">
                <div class="vc_col-sm-12 vc_column_container">
                    <div class="vc_column-inner">
                        <h2 style='text-align: center'>Explore Teachers</h2>
                        <div class="customeffects">
                            <ul>
                                <li><a href="">
                                        <img src="../images/uploads/JONATHAN_SquarePhoto.jpg" class='teachboximg'
                                            alt="">
                                        <div class="teachboxtext">
                                            <h4>JONATHAN</h4>
                                            <p>Early Plucked Instruments</p>
                                            <p>Classical &amp; Acoustic Guitar</p>
                                            <p>Fingerstyle Ukulele</p>
                                            <p>Sitar</p>
                                            <p>Music Theory</p>
                                            <p>Bass &amp; Electric Guitar</p>
                                        </div>
                                    </a>
                                </li>
                                <li><a href="">
                                        <img src="../images/uploads/JONATHAN_SquarePhoto.jpg" class='teachboximg'
                                            alt="">
                                        <div class="teachboxtext">
                                            <h4>JONATHAN</h4>
                                            <p>Early Plucked Instruments</p>
                                            <p>Classical &amp; Acoustic Guitar</p>
                                            <p>Fingerstyle Ukulele</p>
                                            <p>Sitar</p>
                                            <p>Music Theory</p>
                                            <p>Bass &amp; Electric Guitar</p>
                                        </div>
                                    </a>
                                </li>
                                <li><a href="">
                                        <img src="../images/uploads/JONATHAN_SquarePhoto.jpg" class='teachboximg'
                                            alt="">
                                        <div class="teachboxtext">
                                            <h4>JONATHAN</h4>
                                            <p>Early Plucked Instruments</p>
                                            <p>Classical &amp; Acoustic Guitar</p>
                                            <p>Fingerstyle Ukulele</p>
                                            <p>Sitar</p>
                                            <p>Music Theory</p>
                                            <p>Bass &amp; Electric Guitar</p>
                                        </div>
                                    </a>
                                </li>
                                <li><a href="">
                                        <img src="../images/uploads/JONATHAN_SquarePhoto.jpg" class='teachboximg'
                                            alt="">
                                        <div class="teachboxtext">
                                            <h4>JONATHAN</h4>
                                            <p>Early Plucked Instruments</p>
                                            <p>Classical &amp; Acoustic Guitar</p>
                                            <p>Fingerstyle Ukulele</p>
                                            <p>Sitar</p>
                                            <p>Music Theory</p>
                                            <p>Bass &amp; Electric Guitar</p>
                                        </div>
                                    </a>
                                </li>
                                <li><a href="">
                                        <img src="../images/uploads/JONATHAN_SquarePhoto.jpg" class='teachboximg'
                                            alt="">
                                        <div class="teachboxtext">
                                            <h4>JONATHAN</h4>
                                            <p>Early Plucked Instruments</p>
                                            <p>Classical &amp; Acoustic Guitar</p>
                                            <p>Fingerstyle Ukulele</p>
                                            <p>Sitar</p>
                                            <p>Music Theory</p>
                                            <p>Bass &amp; Electric Guitar</p>
                                        </div>
                                    </a>
                                </li>
                                <li><a href="">
                                        <img src="../images/uploads/JONATHAN_SquarePhoto.jpg" class='teachboximg'
                                            alt="">
                                        <div class="teachboxtext">
                                            <h4>JONATHAN</h4>
                                            <p>Early Plucked Instruments</p>
                                            <p>Classical &amp; Acoustic Guitar</p>
                                            <p>Fingerstyle Ukulele</p>
                                            <p>Sitar</p>
                                            <p>Music Theory</p>
                                            <p>Bass &amp; Electric Guitar</p>
                                        </div>
                                    </a>
                                </li>
                                <li><a href="">
                                        <img src="../images/uploads/JONATHAN_SquarePhoto.jpg" class='teachboximg'
                                            alt="">
                                        <div class="teachboxtext">
                                            <h4>JONATHAN</h4>
                                            <p>Early Plucked Instruments</p>
                                            <p>Classical &amp; Acoustic Guitar</p>
                                            <p>Fingerstyle Ukulele</p>
                                            <p>Sitar</p>
                                            <p>Music Theory</p>
                                            <p>Bass &amp; Electric Guitar</p>
                                        </div>
                                    </a>
                                </li>
                                <li><a href="">
                                        <img src="../images/uploads/JONATHAN_SquarePhoto.jpg" class='teachboximg'
                                            alt="">
                                        <div class="teachboxtext">
                                            <h4>JONATHAN</h4>
                                            <p>Early Plucked Instruments</p>
                                            <p>Classical &amp; Acoustic Guitar</p>
                                            <p>Fingerstyle Ukulele</p>
                                            <p>Sitar</p>
                                            <p>Music Theory</p>
                                            <p>Bass &amp; Electric Guitar</p>
                                        </div>
                                    </a>
                                </li>
                                <li><a href="">
                                        <img src="../images/uploads/JONATHAN_SquarePhoto.jpg" class='teachboximg'
                                            alt="">
                                        <div class="teachboxtext">
                                            <h4>JONATHAN</h4>
                                            <p>Early Plucked Instruments</p>
                                            <p>Classical &amp; Acoustic Guitar</p>
                                            <p>Fingerstyle Ukulele</p>
                                            <p>Sitar</p>
                                            <p>Music Theory</p>
                                            <p>Bass &amp; Electric Guitar</p>
                                        </div>
                                    </a>
                                </li>
                                <li><a href="">
                                        <img src="../images/uploads/JONATHAN_SquarePhoto.jpg" class='teachboximg'
                                            alt="">
                                        <div class="teachboxtext">
                                            <h4>JONATHAN</h4>
                                            <p>Early Plucked Instruments</p>
                                            <p>Classical &amp; Acoustic Guitar</p>
                                            <p>Fingerstyle Ukulele</p>
                                            <p>Sitar</p>
                                            <p>Music Theory</p>
                                            <p>Bass &amp; Electric Guitar</p>
                                        </div>
                                    </a>
                                </li>
                                <li><a href="">
                                        <img src="../images/uploads/JONATHAN_SquarePhoto.jpg" class='teachboximg'
                                            alt="">
                                        <div class="teachboxtext">
                                            <h4>JONATHAN</h4>
                                            <p>Early Plucked Instruments</p>
                                            <p>Classical &amp; Acoustic Guitar</p>
                                            <p>Fingerstyle Ukulele</p>
                                            <p>Sitar</p>
                                            <p>Music Theory</p>
                                            <p>Bass &amp; Electric Guitar</p>
                                        </div>
                                    </a>
                                </li>
                                <li><a href="../teachersbook/jonathan/index.html">
                                        <img src="../images/uploads/JONATHAN_SquarePhoto.jpg" class='teachboximg'
                                            alt="">
                                        <div class="teachboxtext">
                                            <h4>JONATHAN</h4>
                                            <p>Early Plucked Instruments</p>
                                            <p>Classical &amp; Acoustic Guitar</p>
                                            <p>Fingerstyle Ukulele</p>
                                            <p>Sitar</p>
                                            <p>Music Theory</p>
                                            <p>Bass &amp; Electric Guitar</p>
                                        </div>
                                    </a>
                                </li>
                            </ul>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
    <?php require_once("../include/footer.php")?>
</body>

</html>