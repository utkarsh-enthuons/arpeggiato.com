<!DOCTYPE html>
<html lang="en">

<head>
    <?php require_once("../include/head.php")?>
</head>

<body class='scheme_original'>
    <?php require_once("../include/header.php")?>
    <div class="page_content">
        <div class="music-page">
            <div class="content_wrap">
                <section class="tab_score music-box">
                    <div class="vc_row">
                        <div class="vc_column-container vc_col-sm-12">
                            <div class="vc_column_inner">
                                <div class="sec_title">
                                    <h3>ARRANGEMENTS FOR SOLO GUITAR</h3>
                                </div>
                                <div class="prop-cat">
                                    <h4>Tab + score</h4>
                                </div>
                                <ul class="products columns-3">
                                    <li class="product_list">
                                        <div class="post_featured"><a href="" class='hover_icon hover_icon_link'><img
                                                    src="../images/uploads/flower_duet_thumbnail-600x338.jpg"
                                                    alt=""></a></div>
                                        <div class="post_content">
                                            <h2 class="product_title">Flower Duet from “Lakme” by Delibes</h2>
                                            <button class="button add_to_cart">Buy Now</button>
                                        </div>
                                    </li>
                                    <li class="product_list">
                                        <div class="post_featured"><a href="" class='hover_icon hover_icon_link'><img
                                                    src="../images/uploads/flower_duet_thumbnail-600x338.jpg"
                                                    alt=""></a></div>
                                        <div class="post_content">
                                            <h2 class="product_title">Flower Duet from “Lakme” by Delibes</h2>
                                            <a href="" class="button add_to_cart">Buy Now</a>
                                        </div>
                                    </li>
                                    <li class="product_list">
                                        <div class="post_featured"><a href="" class='hover_icon hover_icon_link'><img
                                                    src="../images/uploads/flower_duet_thumbnail-600x338.jpg"
                                                    alt=""></a></div>
                                        <div class="post_content">
                                            <h2 class="product_title">Flower Duet from “Lakme” by Delibes</h2>
                                            <a href="" class="button add_to_cart">Buy Now</a>
                                        </div>
                                    </li>
                                    <li class="product_list">
                                        <div class="post_featured"><a href="" class='hover_icon hover_icon_link'><img
                                                    src="../images/uploads/flower_duet_thumbnail-600x338.jpg"
                                                    alt=""></a></div>
                                        <div class="post_content">
                                            <h2 class="product_title">Flower Duet from “Lakme” by Delibes</h2>
                                            <a href="" class="button add_to_cart">Buy Now</a>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </section>
                <section class="score music-box">
                    <div class="vc_row">
                        <div class="vc_column-container vc_col-sm-12">
                            <div class="vc_column_inner">
                                <div class="prop-cat">
                                    <h4>Score</h4>
                                </div>
                                <ul class="products columns-3">
                                    <li class="product_list">
                                        <div class="post_featured"><a href="" class='hover_icon hover_icon_link'><img
                                                    src="../images/uploads/flower_duet_thumbnail-600x338.jpg"
                                                    alt=""></a></div>
                                        <div class="post_content">
                                            <h2 class="product_title">Flower Duet from “Lakme” by Delibes</h2>
                                            <a href="" class="button add_to_cart">Buy Now</a>
                                        </div>
                                    </li>
                                    <li class="product_list">
                                        <div class="post_featured"><a href="" class='hover_icon hover_icon_link'><img
                                                    src="../images/uploads/flower_duet_thumbnail-600x338.jpg"
                                                    alt=""></a></div>
                                        <div class="post_content">
                                            <h2 class="product_title">Flower Duet from “Lakme” by Delibes</h2>
                                            <a href="" class="button add_to_cart">Buy Now</a>
                                        </div>
                                    </li>
                                    <li class="product_list">
                                        <div class="post_featured"><a href="" class='hover_icon hover_icon_link'><img
                                                    src="../images/uploads/flower_duet_thumbnail-600x338.jpg"
                                                    alt=""></a></div>
                                        <div class="post_content">
                                            <h2 class="product_title">Flower Duet from “Lakme” by Delibes</h2>
                                            <a href="" class="button add_to_cart">Buy Now</a>
                                        </div>
                                    </li>
                                    <li class="product_list">
                                        <div class="post_featured"><a href="" class='hover_icon hover_icon_link'><img
                                                    src="../images/uploads/flower_duet_thumbnail-600x338.jpg"
                                                    alt=""></a></div>
                                        <div class="post_content">
                                            <h2 class="product_title">Flower Duet from “Lakme” by Delibes</h2>
                                            <a href="" class="button add_to_cart">Buy Now</a>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </section>
                <section class="tab music-box">
                    <div class="vc_row">
                        <div class="vc_column-container vc_col-sm-12">
                            <div class="vc_column_inner">
                                <div class="prop-cat">
                                    <h4>Tab</h4>
                                </div>
                                <ul class="products columns-3">
                                    <li class="product_list">
                                        <div class="post_featured"><a href="" class='hover_icon hover_icon_link'><img
                                                    src="../images/uploads/flower_duet_thumbnail-600x338.jpg"
                                                    alt=""></a></div>
                                        <div class="post_content">
                                            <h2 class="product_title">Flower Duet from “Lakme” by Delibes</h2>
                                            <a href="" class="button add_to_cart">Buy Now</a>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </section>
                <section class="tag_line music-box">
                    <div class="vc_row">
                        <div class="vc_column-container vc_col-sm-12">
                            <div class="vc_column_inner">
                                <div class="sec_title">
                                    <h3>ARRANGEMENTS FOR VOICE AND GUITAR</h3>
                                </div>
                                <ul class="products columns-3">
                                    <li class="product_list">
                                        <div class="post_featured"><a href="" class='hover_icon hover_icon_link'><img
                                                    src="../images/uploads/flower_duet_thumbnail-600x338.jpg"
                                                    alt=""></a></div>
                                        <div class="post_content">
                                            <h2 class="product_title">Flower Duet from “Lakme” by Delibes</h2>
                                            <a href="" class="button add_to_cart">Buy Now</a>
                                        </div>
                                    </li>
                                    <li class="product_list">
                                        <div class="post_featured"><a href="" class='hover_icon hover_icon_link'><img
                                                    src="../images/uploads/flower_duet_thumbnail-600x338.jpg"
                                                    alt=""></a></div>
                                        <div class="post_content">
                                            <h2 class="product_title">Flower Duet from “Lakme” by Delibes</h2>
                                            <a href="" class="button add_to_cart">Buy Now</a>
                                        </div>
                                    </li>
                                </ul>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </div>
    <?php require_once("../include/footer.php")?>
</body>

</html>