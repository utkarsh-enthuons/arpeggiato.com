<!DOCTYPE html>
<html lang="en">

<head>
    <?php require_once("../include/head.php")?>
</head>

<body class='scheme_original'>
    <?php require_once("../include/header.php")?>
    <section class="page_content">
        <div class="guitar_workshop master-banner-bg">
            <div class="content_wrap">
                <div class="vc_row" data-vc-full-width="true">
                    <div class='vc_column_container vc_col-sm-12'>
                        <div class="vc_column-inner">
                            <div class="master-title">
                                <h2>GUITAR WORKSHOP</h2>
                            </div>
                        </div>
                    </div>
                    <div class="vc_col-sm-6 vc_column-container">
                        <div class="vc_column-inner">
                            <div class="master_title">
                                <p>
                                    <span class='for-highlight'>September 11th, at 1.30 pm Central Time</span>
                                    <span>Admission ticket: <b>$15</b></span>
                                </p>
                            </div>
                            <div class="master_title">
                                <h3>WITH <br> BRANDON ACKER</h3>
                            </div>
                            <div class="time_circles">
                                <div class="textDiv_Days">
                                    <h4>DAYS</h4>
                                    <span>NaN</span>
                                </div>
                                <div class="textDiv_Days">
                                    <h4>HOURS</h4>
                                    <span>NaN</span>
                                </div>
                                <div class="textDiv_Days">
                                    <h4>MINUTES</h4>
                                    <span>NaN</span>
                                </div>
                                <div class="textDiv_Days">
                                    <h4>SECONDS</h4>
                                    <span>NaN</span>
                                </div>
                            </div>
                            <div class="vc_btn3-container"><a href="" class="vc_btn3">Buy Your Ticket</a></div>
                        </div>
                    </div>
                    <div class="vc_col-sm-6 vc_column-container">
                        <div class="vc_column-inner">
                            <div class="master-img">
                                <img src="../images/uploads/Brandon-Classical-Guitar.jpg" alt="">
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="dont_miss">
        <div class="content_wrap">
            <div class="vc_row vc_column-container">
                <div class="vc_col-sm-8 vc_col-xs-offset-2">
                    <div class="vc_column-inner">
                        <h4>Don’t miss your chance to learn with Brandon Acker!</h4>
                        <h4>Join Arpeggiato’s Director in a <span>LIVE</span> workshop online: <br><span>WHAT:</span> A 45-minute workshop + 30-minute live Q & A <br><span>WHEN:</span>Saturday, September 4th at 1:30pm Central Time <br><span>WHERE:</span>On Zoom (we’ll send you the link by email) <br><span>BONUS:</span>Unlimited re-watch for 10 days to practice all these new tips!</h4>
                        <h4>All levels and ages are welcome!</h4>
                        <h4><img src="../images/uploads/1f449.svg" alt=""><img src="../images/uploads/1f449.svg" alt=""><img src="../images/uploads/1f449.svg" alt="">WATCH OUR PREVIOUS MASTERCLASSES & WORKSHOPS <a href="">HERE</a> and <a href="">HERE</a><img src="../images/uploads/1f448.svg" alt=""><img src="../images/uploads/1f448.svg" alt=""><img src="../images/uploads/1f448.svg" alt=""></h4>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="details">
        <div class="content_wrap">
            <div class="vc_row" data-vc-full-width="true">
                <div class="vc_column-container vc_col-sm-12">
                    <div class="vc_column-inner">
                        <h2>Details</h2>
                        <div class="master-txt">
                            <h4>More details soon!</h4>
                            <h4><span>What happens if I have a last-minute conflict and can’t join the Live Event?</span></h4>
                            <h4>No worries: we send a video of the Live Event to <span>ALL</span> ticket holders! The recording will be sent a few days after the Live Event, and will be available to rewatch for <span>10 days</span>.</h4>
                            <h4><b>IMPORTANT:</b> On Saturday, September 11th, we will send the Zoom link around 12 pm Central Time, so stay tuned and check your spam folder! <br>Contact us promptly at ArpeggiatoMusicSchool@gmail.com if you have not received the Zoom link 45min before the class begins.</h4>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="masterclass">
        <div class="content_wrap">
            <div class="vc_row">
                <div class="vc_column-container vc_col-sm-12">
                    <div class="vc_column-inner">
                        <h4>Enjoying our Workshops and Masterclasses? <a href="">Contact us</a> to buy a Live Event Deal!</h4>
                        <h4>-<span>3</span> live events for <span>$40</span> (you save <span>$5</span>), or <br>-<span>6</span> live events for <span>$75</span> (you save <span>$15</span>), or <br>-<span>12</span> live events for <span>$145</span> (you save <span>$35</span>)</h4>
                        <h4><span>You will be able to come to any live event of your choice, at the rhythm you want! No expiration date.</span><br>Just send us a message when you want to join, at least 24h before the live event begins, and we’ll book a virtual seat for you!</h4>
                        <h4>If you would like to offer a Live Event Deal to a loved one, <a href='' style="color: #800000;">contact us for a gift certificate</a>!</h4>
                        <h3><img src="../images/uploads/1f525.svg" alt=""><img src="../images/uploads/1f525.svg" alt=""><img src="../images/uploads/1f525.svg" alt=""><img src="../images/uploads/1f525.svg" alt=""><img src="../images/uploads/1f525.svg" alt=""><img src="../images/uploads/1f525.svg" alt=""><img src="../images/uploads/1f525.svg" alt=""><img src="../images/uploads/1f525.svg" alt=""><img src="../images/uploads/1f525.svg" alt=""><img src="../images/uploads/1f525.svg" alt=""><img src="../images/uploads/1f525.svg" alt=""><img src="../images/uploads/1f525.svg" alt=""></h3>
                        <p>If you would like to perform in one of our live online masterclasses online, please <a href="">CONTACT US!</a></p>
                        <p>SIGN UP AS AN ARPEGGIATO STUDENT TODAY AND SAVE 60% ON ALL FUTURE MASTERCLASSES & WORKSHOPS!</p>
                        <h4>&nbsp;</h4>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <?php require_once("../include/footer.php")?>
</body>

</html>