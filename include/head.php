<meta charset="UTF-8">
    <meta name="description" content="">
    <meta name="keywords" content="">
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8" />
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <title>Arpeggiato</title>
    <!--[if lt IE 9]>
    <script src="https://oss.maxcdn.com/libs/html5shiv/3.7.0/html5shiv.js"></script>
    <script src="https://oss.maxcdn.com/libs/respond.js/1.4.2/respond.min.js"></script>  
    <![endif]-->
    <link rel="stylesheet" id="melodyschool-font-google-fonts-style-css"
        href="http://fonts.googleapis.com/css?family=Lora:300,300italic,400,400italic,700,700italic|Open+Sans:300,300italic,400,400italic,700,700italic&amp;subset=latin,latin-ext"
        type="text/css" media="all">
    <link rel='stylesheet' href='../css/fontello/css/fontello.css'>
    <link rel="stylesheet" href="../css/style.css">
    <link rel="stylesheet" href="../css/theme.css">
    <link rel="stylesheet" href="../css/responsive.css">
    <link rel="stylesheet" href="../css/animate.css">