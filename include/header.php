<header class="top_panel_wrap top_panel_style_6 scheme_original">
    <div class="top_panel_wrap_inner top_panel_inner_style_6 top_panel_position_above">
        <div class="top_panel_middle">
            <div class="content_wrap">
                <div class="contact_logo">
                    <div class="logo">
                        <a href="../../index.html">
                            <img src="../images/uploads/Logo-with-full-word-White-Transparent-background.png"
                                class="logo_main">
                            <img src="../images/uploads/Logo-with-full-word-White-Transparent-background.png"
                                class="logo_fixed"></a>
                    </div>
                </div>
                <div class="menu_main_wrap">
                    <nav class="menu_main_nav_area">
                        <ul id="menu_main" class="menu_main_nav inited sf-js-enabled sf-arrows">
                            <li><a href="../home/index.html">Home</a></li>
                            <li><a href="../our-teachers/index.html">Teachers</a></li>
                            <li><a href="../events/index.html">Events</a></li>
                            <li class="menu-item menu-item-type-custom menu-item-object-custom menu-item-has-children">
                                <a>E-shop</a>
                                <ul class="sub-menu animated fast fadeOutDown">
                                    <li>
                                        <a href="../music-arrangements/index.html">Music Arrangements</a></li>
                                    <li>
                                        <a href="../cd/index.html">CDs (download only)</a></li>
                                    <li>
                                        <a href="../masterclass_archive/index.html">Masterclass Archive</a></li>
                                    <li>
                                        <a href="../workshop-archive/index.html">Workshop Archive</a></li>
                                </ul>
                            </li>
                            <li><a href="../how-it-works/index.html">HOW IT WORKS</a></li>
                            <li>
                                <a href="index.html" aria-current="page">About Us</a>
                            </li>
                            <li><a href="../faq/index.html">FAQ</a></li>
                            <li><a href="../contact/index.html">Contact</a></li>
                            <li><a href="../my-account/index.html">Log In</a></li>
                            <li><a href="../cart/index.html"><span class="dashicons dashicons-cart"></span></a></li>
                        </ul>
                    </nav>
                </div>
            </div>
        </div>

    </div>
</header>
<div class="header_mobile">
    <div class="content_wrap">
        <div class="menu_button icon-menu"></div>
        <div class="logo">
            <a href="../index.html">
            <img src="../images/uploads/Logo-with-full-word-White-Transparent-background.png" class="logo_main"></a>
        </div>
    </div>
    <div class="side_wrap">
        <div class="close">Close</div>
        <div class="panel_top">
            <nav class="menu_main_nav_area">
                <ul id="menu_mobile" class="menu_main_nav">
                    <li><a href="../home/index.html">Home</a></li>
                    <li><a href="../our-teachers/index.html">Teachers</a></li>
                    <li><a href="../events/index.html">Events</a></li>
                    <li><a>E-shop</a>
                        <ul class="sub-menu">
                            <li>
                                <a href="../music-arrangements/index.html">Music Arrangements</a></li>
                            <li>
                                <a href="../cd/index.html">CDs (download only)</a></li>
                            <li>
                                <a href="../masterclass_archive/index.html">Masterclass Archive</a></li>
                            <li>
                                <a href="../workshop-archive/index.html">Workshop Archive</a></li>
                        </ul>
                    </li>
                    <li><a href="../how-it-works/index.html">HOW IT WORKS</a></li>
                    <li><a href="index.html" aria-current="page">About Us</a></li>
                    <li><a href="../faq/index.html">FAQ</a></li>
                    <li><a href="../contact/index.html">Contact</a></li>
                    <li><a href="../my-account/index.html">Log In</a></li>
                    <li><a href="../cart/index.html"><span class="dashicons dashicons-cart"></span></a></li>
                </ul>
            </nav>
        </div>
        <div class="panel_bottom">
        </div>
    </div>
    <div class="mask"></div>
</div>